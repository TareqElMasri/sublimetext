## Sublime text configurations

### Installation
## Ubuntu:
- `cd ~/.config/sublime-text-3/Packages`
- `zip -r user_backup User`
- `cd User && sudo rm -rf *`
- `git clone git@bitbucket.org:TareqElMasri/sublimetext.git ./`
- `sudo cp ~/.bashrc ~/.bashrc_backup && sudo echo $'\n# Sublime settings \n alias subl.push="cd ~/.config/sublime-text-3/Packages/User && git pull origin master && git add . && git commit -a -m \'Updating configurations\' && git push origin master" \n alias subl.pull="cd ~/.config/sublime-text-3/Packages/User && git pull origin master"' >> ~/.bashrc && source ~/.bashrc`
## Mac:
- `cd ~/Library/Application\ Support/Sublime\ Text\ 3/Packages/ `
- `zip -r user_backup User`
- `cd User && sudo rm -rf *`
- `git clone git@bitbucket.org:TareqElMasri/sublimetext.git ./`
- `sudo cp ~/.bash_profile ~/.bash_profile_backup && sudo echo $'\n# Sublime settings \n alias subl.push="~/Library/Application\ Support/Sublime\ Text\ 3/Packages/User && git pull origin master && git add . && git commit -a -m \'Updating configurations\' && git push origin master" \n alias subl.pull="cd ~/Library/Application\ Support/Sublime\ Text\ 3/Packages/User && git pull origin master"' >> ~/.bash_profile && source ~/.bash_profile`

### Usage
- `subl.pull` to get the latest configurations
- `subl.push` to save any changes

### NOTE
- Use `git config credential.helper store` to store git password
- After pulling changes sublime will temporarily crash, don't panic, it will handle the new configurations.

### Anything gone wrong?
In case if anything gone wrong use these commands.

- Restoring sublime:
- `cd ~/.config/sublime-text-3/Packages && sudo rm -rf User && unzip user_backup.zip -d ./`
- Restoring bashrc "Ubuntu": 
- `sudo cp ~/.bashrc ~/.bashrc_backup_2 && sudo cp ~/.bashrc_backup ~/.bashrc`
- Restoring bash_profile "Mac":
- `sudo cp ~/.bash_profile ~/.bash_profile_backup_2 && sudo cp ~/.bash_profile_backup ~/.bash_profile`

Note: Restoring bashrc/bash_profile will restore them at the point you installed the repo, if you have made other configurations to your bash-file, don't use those restore commands.